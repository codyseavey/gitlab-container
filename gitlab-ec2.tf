provider "aws" {}

resource "aws_instance" "gitlab-host" {
    ami                         = "ami-dbc380bb"
    availability_zone           = "us-west-1c"
    instance_type               = "t2.medium"
    associate_public_ip_address = true
    disable_api_termination     = false
    ebs_optimized               = false
    monitoring                  = false
    key_name                    = "seavey_generic"
    subnet_id                   = "subnet-275e707e"
    vpc_security_group_ids      = ["sg-a53473c1"]
    source_dest_check           = true
    iam_instance_profile        = "jenkins"
    root_block_device {
        volume_type           = "gp2"
        volume_size           = 75
        delete_on_termination = true
    }
    connection {
      user = "ec2-user"
      host = "${aws_instance.gitlab-host.public_ip}"
      private_key = "~/.ssh/seavey_generic"
    }
    provisioner "file" {
      source = "./files/create-backup"
      destination = "/home/ec2-user/create-backup"
    }
    provisioner "file" {
      source = "./files/restore-backup.sh"
      destination = "/home/ec2-user/restore-backup.sh"
    }
    provisioner "file" {
      source = "./files/gitlab.rb"
      destination = "/home/ec2-user/gitlab.rb"
    }
    provisioner "file" {
      source = "./files/sudoers"
      destination = "/home/ec2-user/sudoers"
    }
    provisioner "file" {
      source = "./files/sudoers"
      destination = "/home/ec2-user/sudoers"
    }
    provisioner "file" {
      source = "./certs/gitlab.thebelligerentone.com.key"
      destination = "/home/ec2-user/gitlab.thebelligerentone.com.key"
    }
    provisioner "file" {
      source = "./certs/gitlab.thebelligerentone.com.crt"
      destination = "/home/ec2-user/gitlab.thebelligerentone.com.crt"
    }
    provisioner "remote-exec" {
      inline = [
      "sudo chmod u+x /home/ec2-user/create-backup",
      "(crontab -l ; echo \"0 3 * * * /home/ec2-user/create-backup\") | crontab -",
      "sudo bash -c \"cat /home/ec2-user/sudoers &> /etc/sudoers\"",
      "sudo chmod u+x /home/ec2-user/restore-backup.sh",
      "sudo docker run --detach --hostname gitlab.thebelligerentone.com --publish 443:443 --publish 80:80 --publish 10022:22 --name gitlab --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab --volume /srv/gitlab/logs/reconfigure:/var/log/gitlab/reconfigure gitlab/gitlab-ce:8.11.0-ce.1",
      "sleep 40",
      "sudo docker exec -it gitlab update-permissions",
      "sudo docker cp /home/ec2-user/gitlab.rb gitlab:/etc/gitlab/gitlab.rb",
      "sudo docker restart gitlab",
      "sleep 40",
      "sudo docker exec gitlab mkdir -p /etc/gitlab/ssl",
      "sudo docker exec gitlab chmod 700 /etc/gitlab/ssl",
      "sudo docker cp /home/ec2-user/gitlab.thebelligerentone.com.key gitlab:/etc/gitlab/ssl/gitlab.thebelligerentone.com.key",
      "sudo docker cp /home/ec2-user/gitlab.thebelligerentone.com.crt gitlab:/etc/gitlab/ssl/gitlab.thebelligerentone.com.crt",
      "sudo docker exec gitlab gitlab-ctl restart",
      "sudo bash /home/ec2-user/restore-backup.sh",
      ]
    }
    tags {
        "Name" = "Gitlab-EC2"
    }
}
