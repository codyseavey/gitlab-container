gitlab - docker image
---

About this Repostory
---

This repository holds
* A Terraform file(gitlab-ec2.tf) to create a EC2 instance which is our docker host.
* A User-Data script to,
  * Configure gitlab
* A Dockerfile to create gitlab - docker image with all the required tools and configs.

Usage
---

### Gitlab Web URL
* `http://gitlab.thebelligerentone.com:8080`